
[[workspace]]
== Workspaces and Projects

Upon starting the application, you will be presented with a *Workspace Launcher* that 
allows to create a new Workspace or load an existing Workspace from disk. 

A *workspace* allows you to save your Anno configuration files and any JDBC drivers needed
to access your data in one location on your computer.

=== Creating a new Workspace

When you first start the application the you will need to create a new workspace to store
your configuration files.

.Workspace Launcher
image::workspace-launcher.png[role="text-center"]

Click the "Browse" button and navigate to the directory where you wish to create a new 
workspace. You may need to create a new folder for the workspace.

Confirm your selection by pressing "OK".

NOTE: The application will check if the selected folder is empty.

=== Loading an existing workspace

Click the "Browse" button and navigate to the directory with your existing workspace data.  
Confirm your selection by pressing "OK".

NOTE: The application will check if the selected folder is a valid Anno workspace folder.

.Invalid workspace folder selection
image::workspace-launcher-fail.png[role="text-center"]

=== Using the workspace

The workspace acts as a base directory for your configuration and data files. It is good practice
to copy the source files (CSV, Excel) to the workspace folder. This will help you better maintain 
your settings and data files you publish on Genesys. 

=== Under the hood

Anno creates a sub-folder "jdbc" in your workspace. This folder is used as a source location for
any JDBC drivers you may need to to access your databases.


[[project]]
=== Projects

Anno allows you to manage the settings, data sources and data mapping in *Project* files. A project file
contains:

. Server settings, including Genesys server URL, application keys and secrets
. Data sources: CSV, Excel and database queries
. Column configuration and mapping to MCPD

It is good practice to maintain one project file has the configuration used to test the data and push it to the Genesys Sandbox environment, 
and a separate project file is used to publish data to the Genesys production servers.

