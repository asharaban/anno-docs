
[[download]]
== Installing Anno

Anno is an open-source project, licensed under Apache License v2. 
Anno requires the latest Java Run-Time Environment (JRE) to run.

NOTE: Make sure you don't allow installation of browser toolbars like Ask.com in the Java JRE installer.
	Or any other changes to your default browser configuration.

WARNING: Disable Java in all Internet browsers on your computer. Nobody in their right mind would
	still use Java applets in 2016.


If downloading pre-compiled binaries, make sure to download the latest version of Anno for your platform.
You probably have 64-bit CPU and JRE and should use the package labeled `x86_64`. `x86` is for the 32-bit JRE.

Download the package from the https://bitbucket.org/genesys2/anno-swt/downloads[downloads section]
extract if necessary and run the executable for your platform.

[cols="1,4"] 
.Resources
|===
|Project page|https://bitbucket.org/genesys2/anno-swt
|Pre-compiled binaries|https://bitbucket.org/genesys2/anno-swt/downloads
|`git` repository URL|https://bitbucket.org/genesys2/anno-swt.git
|Issue tracker|https://bitbucket.org/genesys2/anno-swt/issues
|===

