
[[trouble]]
== Troubleshooting

This tool is perfect! No way you have a problem! :-)

However, if you do run into trouble using this tool, contact helpdesk@genesys-pgr.org 
for assistance and we will update the tool or this section of the documentation with
resolutions to commonly encountered problems. 



