
[[window]]
== Window Layout

After workspace selection, the main application window is loaded. The window has four sections:

* Toolbar (top)
* List of Data sources (left)
* Data source view (center)
* MCPD descriptor list (right)

.Application Window
image::anno-blank.png[role="text-center"]


=== Toolbar

The toolbar provides access to top-level functions.

.Anno Toolbar
image::toolbar.png[role="text-center"]

[cols="1,4", options="header"] 
.Toolbar buttons
|===
|Label|Description
|Load|Load an existing project file
|Save|Save the current project to a file
|Settings|Opens the Settings dialog
|Add file|Add a new data source file to the project
|Automap|Automatically map columns of the currently open dataset to MCPD descriptors
|Push|Opens a dialog to send data to Genesys
|Add database|Add a new database-backed data source to the project
|===

